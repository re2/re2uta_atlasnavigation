/**
 *
 * floorSegmenter: Segments the floor using PCL
 *
 * rosrun uta_drc_robotSense floorSegmenter
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created 03/14/2013
 * @modified 05/21/2013
 *
 */

#include <ros/ros.h>

// Messages
#include "sensor_msgs/PointCloud.h"
#include <std_msgs/Float64.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/OccupancyGrid.h>

#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>

#include <pcl/surface/concave_hull.h>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/gp3.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <tf/transform_listener.h>

//#include <pcl_ros/transforms.h>

//#include <pcl/visualization/pcl_visualizer.h>
//#include <pcl/visualization/cloud_viewer.h>

// OpenCV stuff
static const char WINDOW[] = "Image window";

class floorSegmenter {

     void scanCb(const sensor_msgs::PointCloud2::ConstPtr& scan);
     void poseCb(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& poseMsg);
     void flooReset_callback(const std_msgs::Float64& msg);
     void occuReset_callback(const std_msgs::Float64& msg);

     public:
              floorSegmenter()
              {
                  ROS_INFO("[floorSegmenter] Initializing...");
                atlas_floor_publisher_ = node_.advertise<sensor_msgs::PointCloud2> ("/atlas_floorPlane", 1, false);
                atlas_floorchull_publisher_ = node_.advertise<geometry_msgs::PolygonStamped> ("/atlas_floorPlaneChull", 1, false);
                atlas_floorcluster_publisher_ = node_.advertise<sensor_msgs::PointCloud2> ("/atlas_floorPlaneCluster", 1, false);
                atlas_floorsmooth_publisher_ = node_.advertise<sensor_msgs::PointCloud2> ("/atlas_floorPlaneSmooth", 1, false);
                atlas_occupancygrid_publisher_ = node_.advertise<nav_msgs::OccupancyGrid> ("/map", 1, false);

                initialpose_sub_ = node_.subscribe<geometry_msgs::PoseWithCovarianceStamped> ("initialpose", 1, &floorSegmenter::poseCb, this);
                atlas_pointcloud_sub_ = node_.subscribe<sensor_msgs::PointCloud2> ( "/ui/pointCloudHandlerData", 1, &floorSegmenter::scanCb, this );

                atlas_flooReset_sub_ = node_.subscribe("/atlas_flooReset", 1, &floorSegmenter::flooReset_callback, this);
                atlas_occuReset_sub_ = node_.subscribe("/atlas_occuReset", 1, &floorSegmenter::occuReset_callback, this);

                // TODO why does this not work?
//                floorPlanes = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
//                selected_floorPlane = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);

                // For OpenCV visualization
                cv::namedWindow(WINDOW);

                // To make sure we segment floor only after we get the point cloud
                pointCloudReceived_flag = false;

                // To make sure we build occupancy only after segmenting floor
                floorSegmented_flag = false;

                ROS_INFO("[floorSegmenter] Finished initializing!");
              }

              ~floorSegmenter()
              {

                cv::destroyWindow(WINDOW);

              }

     private:

        ros::NodeHandle node_;

        ros::Publisher atlas_floor_publisher_, atlas_floorchull_publisher_, atlas_floorcluster_publisher_, atlas_floorsmooth_publisher_, atlas_occupancygrid_publisher_;
        ros::Subscriber atlas_pointcloud_sub_, initialpose_sub_, atlas_flooReset_sub_, atlas_occuReset_sub_;

        geometry_msgs::PoseWithCovarianceStamped pose;

        sensor_msgs::PointCloud2 pubfloorPlane;
        sensor_msgs::PointCloud2 pointCloud;

        nav_msgs::OccupancyGrid occuGrid;

        pcl::PointCloud<pcl::PointXYZ> floorPlanes, selected_floorPlane;

        tf::TransformListener tf_listener;

        bool pointCloudReceived_flag;
        bool floorSegmented_flag;

};

void floorSegmenter::flooReset_callback( const std_msgs::Float64& msg )
{

  // To make sure we segment floor only after we get the point cloud
  if ( !pointCloudReceived_flag )
  {
    pointCloudReceived_flag = false;
    return;
  }

  ROS_INFO("flooReset_callback");

  sensor_msgs::PointCloud2 scan2, floorPlane_cluster, floorPlane_tri;
  geometry_msgs::PolygonStamped floorPlane_chull;

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>), cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>),
                                      cloud_projected(new pcl::PointCloud<pcl::PointXYZ>),
                                      floor_cluster(new pcl::PointCloud<pcl::PointXYZ>);


  /*
   * Update with latest Point Cloud from /ui/pointCloudHandlerData topic
   */
  pcl::fromROSMsg( pointCloud, *cloud );

//  // FIXME need to change this to Odom
//  tf_listener.waitForTransform("/pelvis", (*cloud).header.frame_id,
//  (*cloud).header.stamp, ros::Duration(5.0));  //return value always false!
//    pcl_ros::transformPointCloud("/pelvis", *cloud, *cloud, tf_listener);


  ///////////////////////////////////////////
  // FILTER
  ///////////////////////////////////////////

  // Perform filtering
  pcl::VoxelGrid<pcl::PointXYZ> sor;
  sor.setInputCloud( cloud );
  sor.setLeafSize (0.01, 0.01, 0.01);
  sor.filter ( *cloud_filtered );

// FIXME need to add this back after figuring out which is the floor TF
//  // Build a filter to remove planes away from floor
//  pcl::PassThrough<pcl::PointXYZ> pass;
//  pass.setInputCloud (cloud_filtered);
//  pass.setFilterFieldName ("z");
//  pass.setFilterLimits (-0.1, 0.1);
//  pass.filter (*cloud_filtered);


  ///////////////////////////////////////////
  // PLANE SEGMENTATION
  ///////////////////////////////////////////

  // Plane segmentation
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;

  // Optional
  seg.setOptimizeCoefficients (true);

  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  // scanPCL.makeShared () will make scanPCL a shared pointer
  seg.setInputCloud (cloud_filtered);
  seg.segment (*inliers, *coefficients);

  ROS_DEBUG_STREAM( "Model coefficients: " << coefficients->values[0] << " "
                                          << coefficients->values[1] << " "
                                          << coefficients->values[2] << " "
                                          << coefficients->values[3] << " Inlier indices: "<< inliers->indices.size()) ;
//
//   std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;
//   for (size_t i = 0; i < inliers->indices.size (); ++i)
//       std::cerr << inliers->indices[i] << "    " << scanPCL.points[inliers->indices[i]].x << " "
//                                                  << scanPCL.points[inliers->indices[i]].y << " "
//                                                  << scanPCL.points[inliers->indices[i]].z << std::endl;

  floorPlanes.points.resize(inliers->indices.size());

  for (size_t i = 0; i < inliers->indices.size (); ++i)
    floorPlanes.points[i] = cloud_filtered->points[inliers->indices[i]];

  selected_floorPlane = floorPlanes;
  pcl::toROSMsg(*cloud, pubfloorPlane);

  pubfloorPlane.header = pointCloud.header;

  // Publish the data
  atlas_floor_publisher_.publish( pubfloorPlane );

  floorSegmented_flag = true;

}

void floorSegmenter::occuReset_callback( const std_msgs::Float64& msg )
{
  // To make sure we build occupancy only after segmenting floor
  if ( !floorSegmented_flag )
  {
    floorSegmented_flag = false;
    return;
  }

  ROS_INFO("occuReset_callback");

  ///////////////////////////////////////////
  // OpenCV stuff
  ///////////////////////////////////////////

                                 //H  //W
  cv::Mat image = cv::Mat::ones(400, 400, CV_8UC1)*100;

//  ROS_INFO("Pose x: %f y: %f | Width: %d | Height: %d", pose.pose.pose.position.x, pose.pose.pose.position.y, image.cols, image.rows);

  // TODO make this more efficient
  for (unsigned i=0; i < selected_floorPlane.size(); ++i){
      geometry_msgs::Point32 p;
      p.y = (selected_floorPlane.points[i].x - pose.pose.pose.position.x);
      p.x = (selected_floorPlane.points[i].y - pose.pose.pose.position.y + 5);
      p.z = selected_floorPlane.points[i].z;

//      ROS_INFO("X: %f | Y: %f", p.x, p.y);

      if( (p.x < 10) && (p.y < 10) && (p.x > 0) && (p.y > 0) )
      {
        //ROS_INFO("X: %f | Y: %f", p.x, p.y);
        image.at<uchar>(cv::Point((int)(p.x*40),(int)(p.y*40))) = 0;
      }

    }

//  // TODO Recursive implementation maybe
//  // TODO rows/cols may be flipped
//  // TODO fix this crude 1D filter
//  for (int i=0; i < 400; ++i)
//  {
//    for (int j=2; j < (400 - 0); ++j)
//    {
//      int occuTest = 1;
//      while(occuTest < 2)
//      {
//        if( image.at<uchar>(cv::Point(i,(j - occuTest))) == 0 )
//        {
//          for(int k = occuTest; k > 0; --k)
//          {
//            image.at<uchar>(cv::Point(i,(j + k))) = 0;
//            //ROS_INFO("i: %d j: %d |Testing: %d | K: %d", i, j, image.at<uchar>(cv::Point(i,(j + occuTest))), k);
//          }
//          occuTest = 2;
//        }
//
//        occuTest+=1;
//      }
//    }
//  }

  // TODO Recursive implementation maybe
  // TODO rows/cols may be flipped
  // TODO fix this crude 1D filter
  for (int i=0; i < 400; ++i)
  {
    for (int j=0; j < (400 - 5); ++j)
    {
      int occuTest = 1;
      while(occuTest < 5)
      {
        if( image.at<uchar>(cv::Point((j + occuTest), i)) == 0 )
        {
          for(int k = occuTest; k > 0; --k)
          {
            image.at<uchar>(cv::Point((j + k), i)) = 0;
            //ROS_INFO("i: %d j: %d |Testing: %d | K: %d", i, j, image.at<uchar>(cv::Point(i,(j + occuTest))), k);
          }
          occuTest = 5;
        }

        occuTest+=1;
      }
    }
  }


  ///////////////////////////////////////////
  // END OpenCV stuff
  ///////////////////////////////////////////

  nav_msgs::OccupancyGrid local_occuGrid;

  // This does special magic to append some space behind the robot
  local_occuGrid.info.resolution = 0.025;
  local_occuGrid.info.width = image.cols + 10;
  local_occuGrid.info.height = image.rows;

  // TODO Better way to do this
  //occuGrid.data.resize(occuGrid.info.width*occuGrid.info.height);
  // TODO rows/cols may be flipped
  for (unsigned i=0; i < local_occuGrid.info.height; ++i)
  {
    for (unsigned j=0; j < local_occuGrid.info.width; ++j)
    {
      if( (i < 190 ) || ( i > 210) || ( j > 18) )
      {
        // FIXME need to fix this segfault!!
        //local_occuGrid.data.push_back( image.at<uchar>(cv::Point( i, (j-10) )));
        local_occuGrid.data.push_back(0);
      }else{
        local_occuGrid.data.push_back(0);
      }
    }
  }

  occuGrid = local_occuGrid;
  occuGrid.header = pubfloorPlane.header;
  occuGrid.info.map_load_time = ros::Time::now();
  occuGrid.info.origin = pose.pose.pose;
  occuGrid.info.origin.position.y = occuGrid.info.origin.position.y - 5;
  occuGrid.info.origin.position.x = occuGrid.info.origin.position.x - 0.25;

  atlas_occupancygrid_publisher_.publish(occuGrid);

}

void floorSegmenter::poseCb( const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& poseMsg )
{
  pose = *poseMsg;

  ROS_DEBUG("Pose Received!");
}

void floorSegmenter::scanCb( const sensor_msgs::PointCloud2::ConstPtr & scan )
{

  // FIXME make this more efficient by using pointer instead
  ROS_INFO("[floorSegmenter] Point Cloud Received!");

  pointCloudReceived_flag = true;

  pointCloud = *scan;

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "Atlas_floorSegmenter");

    floorSegmenter floorSegmenter;

    ros::spin();

    return 0;
}
