/**
 *
 * odomTf: This file will generate a world frame using ground truth
 *
 * rosrun uta_drc_robotWalk odomTf
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see http://www.ros.org/wiki/tf/Tutorials/Using%20Stamped%20datatypes%20with%20tf%3A%3AMessageFilter
 * @created 01/09/2013
 * @modified 01/20/2013
 *
 */

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_msgs/Empty.h>

class atlas_tf
{
 public:
          atlas_tf()
          {
            // create ROS topics
            ground_truth_odom_sub_ = node.subscribe("ground_truth_odom", 1, &atlas_tf::odom_callback, this);
            atlas_poseReset_sub_ = node.subscribe("atlas_poseReset", 1, &atlas_tf::poseReset_callback, this);

            initialpose_pub_ = node.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 1, true);

            savedOdom.pose.pose.orientation.x = 0;
            savedOdom.pose.pose.orientation.y = 0;
            savedOdom.pose.pose.orientation.z = 0;
            savedOdom.pose.pose.orientation.w = 1;
            savedOdom.pose.pose.position.x = 0;
            savedOdom.pose.pose.position.y = 0;
            savedOdom.pose.pose.position.z = 0;

            // Init reset to zero
            reset = 0;
          }
 private:

          void poseReset_callback(const std_msgs::Empty& msg)
          {
            reset = 1;
          }

          void odom_callback(const nav_msgs::Odometry& odom)
          {

            worldOdom.sendTransform(
                //This publishes a world TF from the /ground_truth_odom
                tf::StampedTransform(
                        tf::Transform(tf::Quaternion(odom.pose.pose.orientation.x,
                                                     odom.pose.pose.orientation.y,
                                                     odom.pose.pose.orientation.z,
                                                     odom.pose.pose.orientation.w),
                                      tf::Vector3(odom.pose.pose.position.x,
                                                  odom.pose.pose.position.y,
                                                  odom.pose.pose.position.z - 0.923897)),
                        ros::Time::now(),"world", "odom"));

            odomPelvis.sendTransform(
                //This publishes a world TF from the /ground_truth_odom
                tf::StampedTransform(
                        tf::Transform(tf::Quaternion(0,
                                                     0,
                                                     0,
                                                     1),
                                      tf::Vector3(0,
                                                  0,
                                                  0.923897)),
                        ros::Time::now(),"odom", "pelvis"));

            // TODO have a topic that updated odom with new start pose location
            initialpose.header = odom.header;
            initialpose.header.frame_id = "odom";
            initialpose.pose.pose.orientation.x = 0;
            initialpose.pose.pose.orientation.y = 0;
            initialpose.pose.pose.orientation.z = 0;
            initialpose.pose.pose.orientation.w = 1;

            initialpose.pose.pose.position.x = 0;
            initialpose.pose.pose.position.y = 0;
            initialpose.pose.pose.position.z = 0;

            if(reset == 1)
            {
              savedOdom = odom;
              reset = 0;
            }

            // TODO Publish pose in future
            initialpose_pub_.publish(initialpose);

            ROS_DEBUG("running %f %f %f | %f %f %f ", odom.pose.pose.orientation.x, odom.pose.pose.orientation.y, odom.pose.pose.orientation.z, odom.pose.pose.orientation.w, odom.pose.pose.position.x, odom.pose.pose.position.y, odom.pose.pose.position.z);
          }

  tf::TransformBroadcaster worldOdom, odomPelvis;
  ros::NodeHandle node;
  ros::Subscriber ground_truth_odom_sub_, atlas_poseReset_sub_;
  ros::Publisher initialpose_pub_;
  geometry_msgs::PoseWithCovarianceStamped initialpose;

  nav_msgs::Odometry savedOdom;

  double reset;

};
int main(int argc, char** argv){
  ros::init(argc, argv, "Atlas_odomPublisher");
  atlas_tf atlasTF;
  ros::spin();
}
